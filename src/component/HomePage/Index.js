import { React, useEffect, useState } from "react";
import { Container, Grid, Box, Typography } from "@material-ui/core";
import { WiNightCloudyWindy } from "react-icons/wi";
import { AiOutlineSearch } from "react-icons/ai";
import "./index.css";
import { getWeatherData } from "./WeatherAPi";

function Index() {
  const [weatherData, setweatherData] = useState(null);
  const [City, setCity] = useState("Kathmandu");
  const date = new Date();
  const dateAsString = date.toString();
  const timezone = dateAsString.match(/\(([^\)]+)\)$/)[1];

  const [Loading, setLoading] = useState(false);
  const getData = async () => {
    try {
      const data = await getWeatherData(City);
      console.log(data);
      console.log("shit");
      setweatherData(data);
      setLoading(false);
    } catch (error) {
      console.log(error.message);
      setLoading(false);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <>
      {weatherData !== null ? (
        <Box className="background-image">
          <Grid container>
            <Grid item xs={8}>
              <Container>
                <Box className="Container">
                  <Box className="Mainheading">
                    <Typography className="logo"> Weather app</Typography>
                  </Box>
                  <Box className="MainContent">
                    <Grid container>
                      <Grid item xs={3}>
                        <h1>
                          {parseFloat(weatherData.main.temp - 272.15).toFixed(
                            0
                          )}{" "}
                          &deg;
                        </h1>
                      </Grid>
                      <Grid item xs={3}>
                        <Box
                          display="flex"
                          justifyContent="center"
                          flexDirection="column"
                          height="100%"
                        >
                          <h2>{weatherData.name}</h2>
                          <p>{dateAsString}</p>
                        </Box>
                      </Grid>
                      <Grid item xs={3}>
                        <Box
                          display="flex"
                          justifyContent="center"
                          flexDirection="column"
                          height="100%"
                        >
                          <p>
                            <WiNightCloudyWindy size="90" />
                          </p>

                          <p>{weatherData.weather[0].main}</p>
                        </Box>
                      </Grid>
                    </Grid>
                  </Box>
                </Box>
              </Container>
            </Grid>
            <Grid item xs={4}>
              <Box className="rightContainer">
                <Box className="content">
                  <Box className="searchbox">
                    <input
                      type="text"
                      value={City}
                      onChange={(e) => setCity(e.target.value)}
                      placeholder="Enter your city name"
                    />
                  </Box>
                  <button
                    className="searchButton"
                    type="button"
                    onClick={() => getData()}
                  >
                    <AiOutlineSearch size="20" />
                  </button>
                </Box>
                <Box className="space">
                  <Box className="Cities">
                    <li> Kathmandu</li>
                    <li> Kathmandu</li>
                    <li> Kathmandu</li>
                    <li> Kathmandu</li>
                    <li> Kathmandu</li>
                  </Box>
                </Box>
                <Box className="space">
                  <h1> Weather Details</h1>
                  <Box className="content">
                    <p> Pressure</p>
                    <p> {weatherData.main.pressure} N/m-2</p>
                  </Box>
                  <Box className="content">
                    <p> Humidity</p>
                    <p> {weatherData.main.humidity} g.kg-1</p>
                  </Box>
                  <Box className="content">
                    <p> Wind</p>
                    <p> {weatherData.wind.speed}Km/hrr</p>
                  </Box>
                  <Box className="content">
                    <p> Country</p>
                    <p>{weatherData.sys.country}</p>
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      ) : null}
    </>
  );
}

export default Index;
